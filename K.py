from datetime import datetime
import time
import math
##https://ssd.jpl.nasa.gov/horizons.cgi?s_time=1#top
##https://ssd.jpl.nasa.gov/sbdb.cgi
##WolframAlpha
##Wikipedia





from tkinter import *
from tkinter import ttk
root = Tk()
root.overrideredirect(True)
X = root.winfo_screenwidth()
Y = root.winfo_screenheight()
xL = 5 * round((X - 416)/5)
yL = 5 * round((Y - 268)/5)
root.geometry('400x90+{}+{}'.format(xL, yL))
root.lift()
root.wm_attributes('-topmost', True)
root.attributes("-alpha", 0.6)
root.wait_visibility(root)

##def Kaŝu():
##    while 1:
##        Mx = root.winfo_pointerx()
##        My = root.winfo_pointery()
##
##        if abs((Mx-xL)) >= 400 and abs((My-yL)) >90:
##            root.wait_visibility(root)
##            root.wm_attributes('-alpha',0.3)
##        if abs((Mx-xL)) <= 400 and abs((My-yL)) <=90:
##            root.wait_visibility(root)
##            root.wm_attributes('-alpha',1)
##        root.update()

def KreuFenestrumon():
    global loko
    global montru
    global kio
    global longo
    global temploko
    global pliĜustigo

    F = Toplevel()
    F.title('Elektoj')
    F.geometry('550x165')
    F.minsize(550, 165)

    def KreuLingvanElektilon():
        LE = Toplevel()
        LE.title('Elektoj pri lingvoj')
        LE.lift()

        Testo = Label(LE, text='Eble, iotage tio ĉi fenestrumo enhavos elektaron da lingvoj, sed ne nun.', padx=10, pady=10)
        Testo.pack()

    LokŜildo = Label(F, text='Mi volas vidi la dekuman tagnokton kaj «horon» ĉe ')
    LokŜildo.place(x=10, y=10)
    LokŜildo2 = Label(F, text='.')
    LokŜildo2.place(x=352, y=10)

    loko = StringVar()
    Loko = ttk.Combobox(F, width=7, justify=CENTER, textvariable=loko)
    Loko['values'] = ('latero')
    Loko.place(x=287, y=10)

    MontrŜildo = Label(F, text='Mi volas vidi ')
    MontrŜildo.place(x=10, y=35)
    MontrŜildo2 = Label(F, text=' liniojn.')
    MontrŜildo2.place(x=100, y=35)

    montru = StringVar()
    Montru = Entry(F, width=2, justify=CENTER, textvariable=montru)
    Montru.place(x=85, y=35)

    KioŜildo = Label(F, text='Mi volas vidi la ')
    KioŜildo.place(x=10, y=60)
    KioŜildo2 = Label(F, text=' linion se ne estas sufiĉe da spaco por ĉiuj.')
    KioŜildo2.place(x=180, y=60)

    kio = StringVar()
    Kio = ttk.Combobox(F, width=11, justify=CENTER, textvariable=kio)
    Kio['values'] = ('kutiman', 'nekutiman')#eble ankaŭ aldoni 'ambaŭ' poste
    Kio.place(x=95, y=60)

    LongŜildo = Label(F, text='Mi volas vidi la tagnoktumon ')
    LongŜildo.place(x=10, y=85)
    LongŜildo2 = Label(F, text=' longan.')
    LongŜildo2.place(x=247, y=85)

    longo = StringVar()
    Longo = ttk.Combobox(F, width=9, justify=CENTER, textvariable=longo)
    Longo['values'] = ('malpli', 'kutime', 'pli')
    Longo.place(x=171, y=85)#rigardu 'Lost'-on.

    TemplokŜildo = Label(F, text='Mi volas vidi la tagnoktumon laŭ la temploko «')
    TemplokŜildo.place(x=10, y=110)
    TemplokŜildo2 = Label(F, text='».')
    TemplokŜildo2.place(x=300, y=110)

    temploko = StringVar()
    Temploko = Entry(F, width=5, justify=CENTER, textvariable=temploko)
    Temploko.place(x=265, y=110)

    PliĜustigoŜildo = Label(F, text='Mi volas vidi la ')
    PliĜustigoŜildo.place(x=10, y=135)
    PliĜustigoŜildo2 = Label(F, text=' ciferojn post la punkto de la tagnoktmilono.')
    PliĜustigoŜildo2.place(x=130, y=135)

    pliĜustigo = StringVar()
    PliĜustigo = Entry(F, width=5, justify=CENTER, textvariable=pliĜustigo)
    PliĜustigo.place(x=97, y=135)

    Skribsistemo = StringVar()
    L = StringVar()
    L.set('La Latine')
    LaLatine = Radiobutton(F, text='La Latine', variable=Skribsistemo, value='LaLatine', command=KreuLingvanElektilon)
    LaLatine.place(x=450, y=10)

Skatolo = Canvas(root, width=400, height=90)
Skatolo.pack()

Vortojn = Label(Skatolo, justify = LEFT, font=('Arial bold', 9))#, bg='white', fg='blue')
Vortojn.place(x=10, y=13)
V = StringVar()
Vortojn['textvariable'] = V
V.set('Bonvolu Atendi')

Ilo = Button(Skatolo, text='⚙', command=KreuFenestrumon)
Ilo.place(x=362, y=10)


global loko
global montru
global kio
global longo
global temploko
global pliĜustigo

F = Toplevel()
F.title('Elektoj')
F.geometry('550x165')
F.minsize(550, 165)

def KreuLingvanElektilon():
    LE = Toplevel()
    LE.title('Elektoj pri lingvoj')
    LE.lift()

    Testo = Label(LE, text='Eble, iotage tio ĉi fenestrumo enhavos elektaron da lingvoj, sed ne nun.', padx=10, pady=10)
    Testo.pack()

LokŜildo = Label(F, text='Mi volas vidi la dekuman tagnokton kaj «horon» ĉe ')
LokŜildo.place(x=10, y=10)
LokŜildo2 = Label(F, text='.')
LokŜildo2.place(x=352, y=10)

loko = StringVar()
Loko = ttk.Combobox(F, width=7, justify=CENTER, textvariable=loko)
Loko['values'] = ('latero', 'marso')
Loko.place(x=287, y=10)

MontrŜildo = Label(F, text='Mi volas vidi ')
MontrŜildo.place(x=10, y=35)
MontrŜildo2 = Label(F, text=' liniojn.')
MontrŜildo2.place(x=100, y=35)

montru = StringVar()
Montru = Entry(F, width=2, justify=CENTER, textvariable=montru)
Montru.place(x=85, y=35)

KioŜildo = Label(F, text='Mi volas vidi la ')
KioŜildo.place(x=10, y=60)
KioŜildo2 = Label(F, text=' linion se ne estas sufiĉe da spaco por ĉiuj.')
KioŜildo2.place(x=180, y=60)

kio = StringVar()
Kio = ttk.Combobox(F, width=11, justify=CENTER, textvariable=kio)
Kio['values'] = ('kutiman', 'nekutiman')#eble ankaŭ aldoni 'ambaŭ' poste
Kio.place(x=95, y=60)

LongŜildo = Label(F, text='Mi volas vidi la tagnoktumon ')
LongŜildo.place(x=10, y=85)
LongŜildo2 = Label(F, text=' longan.')
LongŜildo2.place(x=247, y=85)

longo = StringVar()
Longo = ttk.Combobox(F, width=9, justify=CENTER, textvariable=longo)
Longo['values'] = ('malpli', 'kutime', 'pli')
Longo.place(x=171, y=85)#rigardu 'Lost'-on.

TemplokŜildo = Label(F, text='Mi volas vidi la tagnoktumon laŭ la temploko «')
TemplokŜildo.place(x=10, y=110)
TemplokŜildo2 = Label(F, text='».')
TemplokŜildo2.place(x=300, y=110)

temploko = StringVar()
Temploko = Entry(F, width=5, justify=CENTER, textvariable=temploko)
Temploko.place(x=265, y=110)

PliĜustigoŜildo = Label(F, text='Mi volas vidi la ')
PliĜustigoŜildo.place(x=10, y=135)
PliĜustigoŜildo2 = Label(F, text=' ciferojn post la punkto de la tagnoktmilono.')
PliĜustigoŜildo2.place(x=130, y=135)

pliĜustigo = StringVar()
PliĜustigo = Entry(F, width=5, justify=CENTER, textvariable=pliĜustigo)
PliĜustigo.place(x=97, y=135)

Skribsistemo = StringVar()
L = StringVar()
L.set('La Latine')
LaLatine = Radiobutton(F, text='La Latine', variable=Skribsistemo, value='LaLatine', command=KreuLingvanElektilon)
LaLatine.place(x=450, y=10)

loko.set('la tero')
montru.set('5')
kio.set('kutiman')
longo.set('kutime')
temploko.set('0')
pliĜustigo.set('2')


Praviganto2 = 30.09
Unikso = None



def LaTagumero(Unikso, MilisekundojPerTagumo, TagumojInterEpoĥoKajUnikso, TagumojPerUnuaJararo, moduloUnua, TagumojPerDuaJararo, moduloDua,
               PlejMalsupraKomunaOblo, EnŝovaLongo, NeenŝovaLongo, KiamEnŝoviguJaron, JarDekonaLongo, DekaJardekonaLongo, ĈuFaruMonaton, MonataLongo,
               FinaMonataLongo, Temploko, Loko, Longo, PliĜustigo, Kio, Montru, Praviganto1, Praviganto2):

    PliĜustigo = math.floor(PliĜustigo)
    try:
        time.sleep(0.02/PliĜustigo)
    except Exception:
        time.sleep(0.05)

    KomplementajTagoj = 5
    if Unikso == None:
        Unikso = time.time() * 1000 - Praviganto2
    Tagoj = Unikso / MilisekundojPerTagumo + TagumojInterEpoĥoKajUnikso - Praviganto1 + (Temploko * .1)
    AntaŭUnuaKielero = Tagoj / TagumojPerUnuaJararo
    UnuaKielero = math.floor(AntaŭUnuaKielero)
    AntaŭDuaKielero = (AntaŭUnuaKielero - UnuaKielero) / TagumojPerDuaJararo * TagumojPerUnuaJararo
    DuaKielero = math.floor(AntaŭUnuaKielero)
    JarojEnŝovaj = DuaKielero - UnuaKielero
    JarojNeenŝovaj = PlejMalsupraKomunaOblo - JarojEnŝovaj
    AveraĝaJaro = (JarojEnŝovaj * EnŝovaLongo + JarojNeenŝovaj * NeenŝovaLongo) / PlejMalsupraKomunaOblo

    D_1 = Tagoj / AveraĝaJaro
    if D_1 < 0:
        Jareciganto = -1
    else:
        Jareciganto = 1
    Jaro = math.floor(D_1) + Jareciganto
    D_3 = D_1 - Jaro + Jareciganto

    JaroDaDuaKielero = str(Jaro % DuaKielero)
    JaroDaUnuaKielero = str(Jaro % UnuaKielero)
    ĈuEnŝovo = 0
    if KiamEnŝoviguJaron == -1: # se aŭ unua aŭ dua veras («OR»)
        if JaroDaDuaKielero not in (moduloDua):
            KielLongasLaJaro = NeenŝovaLongo
        if JaroDaUnuaKielero not in (moduloUnua):
            KielLongasLaJaro = NeenŝovaLongo
        if JaroDaDuaKielero in (moduloDua):
            KielLongasLaJaro = EnŝovaLongo
            ĈuEnŝovo = 1
        if JaroDaUnuaKielero in (moduloUnua):
            KielLongasLaJaro = EnŝovaLongo
            ĈuEnŝovo = 1
        Enŝovo = ĈuEnŝovo * EnŝovaLongo
    if KiamEnŝoviguJaron == 0: # se unu el du veras («XOR»)
        if JaroDaDuaKielero in (moduloDua):
            if JaroDaUnuaKielero not in (moduloUnua):
                KielLongasLaJaro = EnŝovaLongo
                ĈuEnŝovo = 1
        if JaroDaDuaKielero in (moduloDua):
            if JaroDaUnuaKielero in (moduloUnua):
                KielLongasLaJaro = NeenŝovaLongo
        if JaroDaUnuaKielero in (moduloUnua):
            if JaroDaDuaKielero not in (moduloDua):
                KielLongasLaJaro = EnŝovaLongo
                ĈuEnŝovo = 1
        if JaroDaUnuaKielero not in (moduloUnua):
            if JaroDaDuaKielero not in (moduloDua):
                KielLongasLaJaro = NeenŝovaLongo
        Enŝovo = ĈuEnŝovo * EnŝovaLongo
    # se la unua el du, aŭ ambaŭ el du, veras
    # se la duua el du, aŭ ambaŭ el du, veras
    # se ambaŭ el du, aŭ nul el du, veras
    LaTagumoj = D_3 * KielLongasLaJaro

    D_4 = LaTagumoj / JarDekonaLongo + 1
    JarDekono = math.floor(D_4)
    D_5 = D_4 - JarDekono
    if JarDekono < 10:
        D_6 = D_5 * JarDekonaLongo
    if JarDekono > 9:
        D_6 = D_5 * (DekaJardekonaLongo + Enŝovo * ĈuEnŝovo)
    if D_6 < 1:
        if JarDekono > 1:
            JarDekono -= 1
            D_6 += JarDekonaLongo
        if JarDekono > 9:
            Jaro -= 1
            ĈuEnŝovo = 0
            if KiamEnŝoviguJaron == -1: # se aŭ unua aŭ dua veras («OR»)
                if JaroDaDuaKielero not in (moduloDua):
                    KielLongasLaJaro = NeenŝovaLongo
                if JaroDaUnuaKielero not in (moduloUnua):
                    KielLongasLaJaro = NeenŝovaLongo
                if JaroDaDuaKielero in (moduloDua):
                    KielLongasLaJaro = EnŝovaLongo
                    ĈuEnŝovo = 1
                if JaroDaUnuaKielero in (moduloUnua):
                    KielLongasLaJaro = EnŝovaLongo
                    ĈuEnŝovo = 1
                Enŝovo = ĈuEnŝovo * EnŝovaLongo
            if KiamEnŝoviguJaron == 0: # se unu el du veras («XOR»)
                if JaroDaDuaKielero in (moduloDua):
                    if JaroDaUnuaKielero not in (moduloUnua):
                        KielLongasLaJaro = EnŝovaLongo
                        ĈuEnŝovo = 1
                if JaroDaDuaKielero in (moduloDua):
                    if JaroDaUnuaKielero in (moduloUnua):
                        KielLongasLaJaro = NeenŝovaLongo
                if JaroDaUnuaKielero in (moduloUnua):
                    if JaroDaDuaKielero not in (moduloDua):
                        KielLongasLaJaro = EnŝovaLongo
                        ĈuEnŝovo = 1
                if JaroDaUnuaKielero not in (moduloUnua):
                    if JaroDaDuaKielero not in (moduloDua):
                        KielLongasLaJaro = NeenŝovaLongo
                Enŝovo = ĈuEnŝovo * EnŝovaLongo
            # se la unua el du, aŭ ambaŭ el du, veras
            # se la duua el du, aŭ ambaŭ el du, veras
            # se ambaŭ el du, aŭ nul el du, veras
            JarDekono = 10
            D_6 += DekaJardekonaLongo + Enŝovo * ĈuEnŝovo
    TagoDeLaJardekono = math.floor(D_6)

    ATagDekono = (D_6 - TagoDeLaJardekono) * 10
    TagDekono = math.floor(ATagDekono)
    TagMilono = (ATagDekono - TagDekono) * 100

    AHoroj = (D_6 - TagoDeLaJardekono) * 24
    Horoj = math.floor(AHoroj)
    AMinutoj = (AHoroj - Horoj) * 60
    Minutoj = math.floor(AMinutoj)
    Sekundoj = (AMinutoj - Minutoj) * 60
    Minutoj = '{:02.0f}'.format(math.floor(Minutoj))
    if PliĜustigo < 2:
        Sekundoj = ''
    else:
        if Sekundoj < 10:
            Sekundoj = ':0{}'.format(math.floor(Sekundoj))
        else:
            Sekundoj = ':{}'.format(math.floor(Sekundoj))
    if PliĜustigo < 1:
        TagMilono = '{:02.0f}'.format(math.floor(TagMilono))
    else:
        PliĜustigon = '0{}.{}'.format((PliĜustigo + 3), PliĜustigo)
        TagMilono = '{:{}f}'.format(TagMilono, PliĜustigon)
        TagMilono = str(TagMilono).replace('.', ',')

    if JarDekono == 1:
        JarDekonoj = 'Unuembro'
    elif JarDekono == 2:
        JarDekonoj = 'Duembro'
    elif JarDekono == 3:
        JarDekonoj = 'Triembro'
    elif JarDekono == 4:
        JarDekonoj = 'Kvarembro'
    elif JarDekono == 5:
        JarDekonoj = 'Kvinembro'
    elif JarDekono == 6:
        JarDekonoj = 'Sesembro'
    elif JarDekono == 7:
        JarDekonoj = 'Sepembro'
    elif JarDekono == 8:
        JarDekonoj = 'Okembro'
    elif JarDekono == 9:
        JarDekonoj = 'Naŭembro'
    else:
        JarDekonoj = 'Dekembro'

    if ĈuEnŝovo >= 1:
        KomplementajTagoj += Enŝovo
    if JarDekono < 10:
        if math.floor(LaTagumoj) % 10 == 0:
            TagoDeTagDeko = 'Dekdo'
        elif math.floor(LaTagumoj) % 10 == 1:
            TagoDeTagDeko = 'Unudo'
        elif math.floor(LaTagumoj) % 10 == 2:
            TagoDeTagDeko = 'Dudo'
        elif math.floor(LaTagumoj) % 10 == 3:
            TagoDeTagDeko = 'Trido'
        elif math.floor(LaTagumoj) % 10 == 4:
            TagoDeTagDeko = 'Kvardo'
        elif math.floor(LaTagumoj) % 10 == 5:
            TagoDeTagDeko = 'Kvindo'
        elif math.floor(LaTagumoj) % 10 == 6:
            TagoDeTagDeko = 'Sesdo'
        elif math.floor(LaTagumoj) % 10 == 7:
            TagoDeTagDeko = 'Sepdo'
        elif math.floor(LaTagumoj) % 10 == 8:
            TagoDeTagDeko = 'Okdo'
        elif math.floor(LaTagumoj) % 10 == 9:
            TagoDeTagDeko = 'Naŭdo'
        else:
            TagoDeTagDeko = '???'
    else:
        if TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj):
            if math.floor(LaTagumoj) % 10 == 0:
                TagoDeTagDeko = 'Dekdo'
            elif math.floor(LaTagumoj) % 10 == 1:
                TagoDeTagDeko = 'Unudo'
            elif math.floor(LaTagumoj) % 10 == 2:
                TagoDeTagDeko = 'Dudo'
            elif math.floor(LaTagumoj) % 10 == 3:
                TagoDeTagDeko = 'Trido'
            elif math.floor(LaTagumoj) % 10 == 4:
                TagoDeTagDeko = 'Kvardo'
            elif math.floor(LaTagumoj) % 10 == 5:
                TagoDeTagDeko = 'Kvindo'
            elif math.floor(LaTagumoj) % 10 == 6:
                TagoDeTagDeko = 'Sesdo'
            elif math.floor(LaTagumoj) % 10 == 7:
                TagoDeTagDeko = 'Sepdo'
            elif math.floor(LaTagumoj) % 10 == 8:
                TagoDeTagDeko = 'Okdo'
            elif math.floor(LaTagumoj) % 10 == 9:
                TagoDeTagDeko = 'Naŭdo'
            else:
                TagoDeTagDeko = '???'
        if TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 1):
            TagoDeTagDeko = 'Festo de Virto'
        elif TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 2):
            TagoDeTagDeko = 'Festo de Genio'
        elif TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 3):
            TagoDeTagDeko = 'Festo de Laboro'
        elif TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 4):
            TagoDeTagDeko = 'Festo de Opinio'
        elif TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 5):
            TagoDeTagDeko = 'Festo de Rekompenco'
        elif TagoDeLaJardekono < (DekaJardekonaLongo - KomplementajTagoj + 6):
            TagoDeTagDeko = 'Festo de Libereco/Revolucio'
        else:
            TagoDeTagDeko = '???'

    if math.floor(Tagoj) % 7 == 0:
        TagoDeLaSemajno = 'Mardo'
    if math.floor(Tagoj) % 7 == 1:
        TagoDeLaSemajno = 'Merkredo'
    if math.floor(Tagoj) % 7 == 2:
        TagoDeLaSemajno = 'Ĵaŭdo'
    if math.floor(Tagoj) % 7 == 3:
        TagoDeLaSemajno = 'Vendredo'
    if math.floor(Tagoj) % 7 == 4:
        TagoDeLaSemajno = 'Sabato'
    if math.floor(Tagoj) % 7 == 5:
        TagoDeLaSemajno = 'Dimanĉo'
    if math.floor(Tagoj) % 7 == 6:
        TagoDeLaSemajno = 'Lundo'

    if Longo == 1 or Longo == -1:
        PliLonga = JarDekonaLongo + (10 * Longo)
        if (math.floor(PliLonga / 10)) * 10 >= (JarDekonaLongo + 5):
            PliLonga = (math.ceil(PliLonga / 10)) * 10

    if ĈuFaruMonaton != 0:
        F_4 = LaTagumoj / MonataLongo + 1
        Monato = math.floor(F_4)
        F_5 = F_4 - Monato
        if Monato < 13:
            F_6 = F_5 * MonataLongo
        else:
            F_6 = F_5 * (FinaMonataLongo + Enŝovo * ĈuEnŝovo)
        if F_6 < 1:
            if Monato > 1:
                Monato -= 1
                F_6 += MonataLongo
            if JarDekono > 9:
                Jaro -= 1
                ĈuEnŝovo = 0
                if KiamEnŝoviguJaron == -1: # se aŭ unua aŭ dua veras («OR»)
                    if JaroDaDuaKielero not in (moduloDua):
                        KielLongasLaJaro = NeenŝovaLongo
                    if JaroDaUnuaKielero not in (moduloUnua):
                        KielLongasLaJaro = NeenŝovaLongo
                    if JaroDaDuaKielero in (moduloDua):
                        KielLongasLaJaro = EnŝovaLongo
                        ĈuEnŝovo = 1
                    if JaroDaUnuaKielero in (moduloUnua):
                        KielLongasLaJaro = EnŝovaLongo
                        ĈuEnŝovo = 1
                    Enŝovo = ĈuEnŝovo * EnŝovaLongo
                if KiamEnŝoviguJaron == 0: # se unu el du veras («XOR»)
                    if JaroDaDuaKielero in (moduloDua):
                        if JaroDaUnuaKielero not in (moduloUnua):
                            KielLongasLaJaro = EnŝovaLongo
                            ĈuEnŝovo = 1
                    if JaroDaDuaKielero in (moduloDua):
                        if JaroDaUnuaKielero in (moduloUnua):
                            KielLongasLaJaro = NeenŝovaLongo
                    if JaroDaUnuaKielero in (moduloUnua):
                        if JaroDaDuaKielero not in (moduloDua):
                            KielLongasLaJaro = EnŝovaLongo
                            ĈuEnŝovo = 1
                    if JaroDaUnuaKielero not in (moduloUnua):
                        if JaroDaDuaKielero not in (moduloDua):
                            KielLongasLaJaro = NeenŝovaLongo
                    Enŝovo = ĈuEnŝovo * EnŝovaLongo
                # se la unua el du, aŭ ambaŭ el du, veras
                # se la duua el du, aŭ ambaŭ el du, veras
                # se ambaŭ el du, aŭ nul el du, veras
                Monato = 13
                F_6 += FinaMonataLongo + Enŝovo * ĈuEnŝovo
        M_Tago = math.floor(F_6)
        if Monato == 1:
            Monatoj = 'Vendemiero'
        elif Monato == 2:
            Monatoj = 'Brumero'
        elif Monato == 3:
            Monatoj = 'Frimero'
        elif Monato == 4:
            Monatoj = 'Nivozo'
        elif Monato == 5:
            Monatoj = 'Pluviozo'
        elif Monato == 6:
            Monatoj = 'Ventozo'
        elif Monato == 7:
            Monatoj = 'Ĝerminalo'
        elif Monato == 8:
            Monatoj = 'Florealo'
        elif Monato == 9:
            Monatoj = 'Prerialo'
        elif Monato == 10:
            Monatoj = 'Mesidoro'
        elif Monato == 11:
            Monatoj = 'Termidoro'
        elif Monato == 12:
            Monatoj = 'Fruktidoro'
        else:
            Monatoj = 'Komplementaj Tagoj'

    Jaro = '{:+d}'.format(Jaro)

    try:
        Templokon = '{:+d}'.format(Temploko)
        Templokon = 'ankaŭ {}'.format(Templokon)
    except Exception:
        if Temploko == -3.4916666667:
            Templokon = 'je Fenikso'
        if Temploko == 1.50833333:
            Templokon = 'duontagume for da Fenikso'
        if Temploko == 3.7302777778:
            Templokon = 'je Vikingo Dua'
        if Temploko == -1.2697222222:
            Templokon = 'duontagume for da Vikingo Dua'
        if Temploko == 1.3319444444:
            Templokon = 'je Vikingo Unua'
        if Temploko == -3.6680555555:
            Templokon = 'duontagume for da Vikingo Unua'
        if Temploko == -0.0369111111:
            Templokon = 'je Vojtrovanto-Vojaĝeganto'
        if Temploko == 4.9630888889:
            Templokon = 'duontagume for da Vojtrovanto-Vojaĝeganto'
        if Temploko == 2.1508333333:
            Templokon = 'je Persistema—Eltrovema'
        if Temploko == -2.8491666667:
            Templokon = 'duontagume for da Persistema—Eltrovema'
        if Temploko == 3.7673166667:
            Templokon = 'je InSight'
        if Temploko == -1.2326833333:
            Templokon = 'duontagume for da InSight'
        if Temploko == 3.8172222222:
            Templokon = 'je Scivolemaĵo'
        if Temploko == -1.1827777778:
            Templokon = 'duontagume for da Scivolemaĵo'
        if Temploko == -0.1535166667:
            Templokon = 'je Opportunity'
        if Temploko == 4.8464833333:
            Templokon = 'duontagume for da Opportunity'
        if Temploko == 4.87423989:
            Templokon = 'je Spirito'
        if Temploko == -0.125760111:
            Templokon = 'duontagume for da Spirito'
        if Temploko == -0.5394444444:
            Templokon = 'je Marso Sesa'
        if Temploko == 4.4605555556:
            Templokon = 'duontagume for da Marso Sesa'
        if Temploko == 4.3888888889:
            Templokon = 'je Marso Tria'
        if Temploko == -0.6111111111:
            Templokon = 'duontagume for da  Marso Tria'
    NulaVico = ('{}, sed {}'.format(Loko, Templokon))
    UnuaVico = ('Jar-dekona Kalendaro: {}, {} {} {} je {},{}'.format(TagoDeTagDeko, TagoDeLaJardekono, JarDekonoj, Jaro, TagDekono, TagMilono))
    DuaVico = ĈuFaruMonaton
    try:
        if -1 < Longo < 1:
            if ĈuFaruMonaton == 0:
                DuaVico = ('Semajnhora Kalendaro: {}, {} {} {} je {}:{}{}'.format(TagoDeLaJardekono, TagoDeLaSemajno, JarDekonoj, Jaro, Horoj, Minutoj, Sekundoj))
            else:
                DuaVico = ('Jar-dektriona Kalendaro: {}, {} {} {} je {},{}'.format(TagoDeTagDeko, M_Tago, Monatoj, Jaro, TagDekono, TagMilono))
    except Exception:
        pass
    if Longo == 1:
        if ĈuFaruMonaton != 0:
            DuaVico = ('Jar-dekona Kalendaro: {}, {} {} {} je {},{}'.format(TagoDeTagDeko, M_Tago, Monatoj, Jaro, AliTagDekono, AliTagMilono))
    if Longo == -1:
        DuaVico = ('Jar-dekona Kalendaro: {}, {} {} {} je {},{}'.format(TagoDeTagDeko, M_Tago, Monatoj, Jaro, AliTagDekono, AliTagMilono))
    if Montru == 4:
        Vortoj = '{}\n\n{}\n{}'.format(NulaVico, UnuaVico, DuaVico)
    if Montru == 5:
        Vortoj = '{}\n\n{}\n{}\n'.format(NulaVico, UnuaVico, DuaVico)
    if Montru == 3:
        Vortoj = '{}\n{}\n{}'.format(NulaVico, UnuaVico, DuaVico)
    if Montru == 2:
        Vortoj = '{}\n{}'.format(UnuaVico, DuaVico)
    if Montru == 1:
        if Kio == 1:
            Vortoj = '{}'.format(UnuaVico)
        else:
            Vortoj = '{}'.format(DuaVico)

    Dormo = int((30 - Unikso + time.time() * 1000 + Praviganto2)/1000)
    if Dormo < 0:
        Dormo = 0
    if Dormo > 30:
        Dormo = 30
    return Vortoj

def Funkciigu():
    while 1:
        try:
            try:
                PliĜustigo = math.floor(int(pliĜustigo.get()))
            except Exception:
                PliĜustigo = 2
            Loko_ = loko.get()
            if Loko_.lower() in ('Marso', 'marso', '499', '450'):
                try:
                    if str(temploko.get()).lower()[0] == '-':
                        if str(temploko.get()).lower()[1:] in ('phoenix', 'fenikso', 'feniks', '2007-034a', '32003'):
                            Temploko = 1.50833333
                        if str(temploko.get()).lower()[1:] in ('viking 2', 'viking two', 'viking2', 'vikingtwo', 'vajkeng 2', 'vajkeng du', 'vajkeng tu', 'vajkeng2', 'vajkengdu', 'vajkengtu', 'vajking 2', 'vajking du', 'vajking tu', 'vajking2', 'vajkingdu', 'vajkingtu', 'vikingo 2a', '2a vikingo', 'vikingo2a', '2avikingo', 'vikingo 2', 'vikingo du', 'vikingo dua', 'vikingo2', 'vikingodu', 'vikingodua', 'dua vikingo', 'duavikingo', '1975-083c', '9408', '1975-083a', '8199'):
                            Temploko = -1.2697222222
                        if str(temploko.get()).lower()[1:] in ('viking 1', 'viking one', 'viking1', 'vikingone', 'vajkeng 1', 'vajkeng unu', 'vajkeng', 'vajkeng1', 'vajkengunu', 'vajking 1', 'vajking unu', 'vajking1', 'vajkingunu', 'vikingo', 'vikingo 1', 'vikingo 1a', '1a vikingo', 'vikingo1a', '1avikingo', 'vikingo unu', 'vikingo unua', 'vikingo1', 'vikingounu', 'vikingounua', 'unua vikingo', 'unuavikingo', '1975-075c', '9024', '1975-075a', '8108'):
                            Temploko = -3.6680555555
                        if str(temploko.get()).lower()[1:] in ('sojourner', 'soĵerner', 'soĵernor', 'soĝerner', 'soĝernor', 'vojaĝeganto', 'mars pathfinder', 'marz pathfinder', 'marsa vojtrovanto', 'marsvojtrovanto', 'mars-vojtrovanto', 'marsovojtrovanto', 'vojtrovanto de marso', 'vojtrovanto je marso', 'vojtrovanto marsa', '1996-068a', '24667' 'pathfinder', 'vojtrovanto'):
                            Temploko = 4.9630888889
                        if str(temploko.get()).lower()[1:] in ('perseverance', 'percy', 'persistemo', 'persistema', 'persevirans', 'persi', 'ingenuity', 'enĝeueti', 'inĝeueti', 'mars helicopter', 'helikoptero marsa', 'marsa helikoptero', 'marshelikoptero', 'mars-helikoptero', 'marsohelikoptero', 'eltrovemo', 'eltrovema'):
                            Temploko = 2.1514111111
                        if str(temploko.get()).lower()[1:] in ('2018-042a', '43457', 'interior exploration using seismic investigations, geodesy and heat transport', 'interior exploration using seismic investigations, geodesy and heat transport (insight)', 'interior exploration using seismic investigations, geodesy, and heat transport', 'interior exploration using seismic investigations, geodesy, and heat transport (insight)', 'interior exploration using seismic investigations geodesy and heat transport', 'interior exploration using seismic investigations geodesy and heat transport (insight)', 'ensajt'):
                            Temploko = -1.2326833333
                        if str(temploko.get()).lower()[1:] in ('curiosity', 'scivolemo', 'scivolema', 'scivolemaĵo', 'scivolemulo', 'kuriozo', 'kurioza', 'kuriozaĵo', 'mars science laboratory', 'msl', '37936', '2011-070a', 'marsa scienca laboratorio', 'scienca laboratorio marsa'):
                            Temploko = -1.1827777778
                        if str(temploko.get()).lower()[1:] in ('opportunity', 'mer-b', 'merb', 'mars exploration rover b', 'esplorada vaganta veturilo marsa b', 'marsa esplorada vaganta veturilo b', 'esplorada veturilo marsa b', 'marsa esplorada veturilo b', 'marz eksplorejŝen rover b', 'marz eksplorejŝan rover b', 'oportjuneti', 'oportuneti', '2003-032a', '27849'):
                            Temploko = 4.8464833333
                        if str(temploko.get()).lower()[1:] in ('spirit', 'spirito', 'animo', 'mer-a', 'mera', 'mars exploration rover a', 'esplorada vaganta veturilo marsa a', 'marsa esplorada vaganta veturilo a', 'esplorada veturilo marsa a', 'marsa esplorada veturilo a', 'marz eksplorejŝen rover a', 'marz eksplorejŝan rover a' '2003-027a', '27827'):
                            Temploko = -0.125760111
                        if str(temploko.get()).lower()[1:] in ('марс-6', 'марс6', 'марс 6', 'м-73п 50', 'mars-6', 'mars 6', 'marz-6', 'marz6', 'marsa 6', 'marsa-6', 'marsa6', 'marsa sesa', 'sesa marsa', 'marso 6', 'marso-6', 'marso6', 'marso sesa', 'sesa marso', '7223', '6768', '1973-052d', '1973-052a'):
                            Temploko = 4.4605555556
                        if str(temploko.get()).lower()[1:] in ('марс-3', 'марс3', 'марс 3', 'mars-3', 'mars 3', 'marz-3', 'marz3', 'marsa 3', 'marsa-3', 'marsa3', 'marsa tria', 'tria marsa', 'marso 3', 'marso-3', 'marso3', 'marso tria', 'tria marso', '5667', '5252', '1971-049c', '1971-049a'):
                            Temploko = -0.6111111111
                    elif str(temploko.get()).lower()[0:2] in ('mal', '180', '200'):
                        if str(temploko.get()).lower()[3:] in ('phoenix', 'fenikso', 'feniks', '2007-034a', '32003'):
                            Temploko = 1.50833333
                        if str(temploko.get()).lower()[3:] in ('viking 2', 'viking two', 'viking2', 'vikingtwo', 'vajkeng 2', 'vajkeng du', 'vajkeng tu', 'vajkeng2', 'vajkengdu', 'vajkengtu', 'vajking 2', 'vajking du', 'vajking tu', 'vajking2', 'vajkingdu', 'vajkingtu', 'vikingo 2a', '2a vikingo', 'vikingo2a', '2avikingo', 'vikingo 2', 'vikingo du', 'vikingo dua', 'vikingo2', 'vikingodu', 'vikingodua', 'dua vikingo', 'duavikingo', '1975-083c', '9408', '1975-083a', '8199'):
                            Temploko = -1.2697222222
                        if str(temploko.get()).lower()[3:] in ('viking 1', 'viking one', 'viking1', 'vikingone', 'vajkeng 1', 'vajkeng unu', 'vajkeng', 'vajkeng1', 'vajkengunu', 'vajking 1', 'vajking unu', 'vajking1', 'vajkingunu', 'vikingo', 'vikingo 1', 'vikingo 1a', '1a vikingo', 'vikingo1a', '1avikingo', 'vikingo unu', 'vikingo unua', 'vikingo1', 'vikingounu', 'vikingounua', 'unua vikingo', 'unuavikingo', '1975-075c', '9024', '1975-075a', '8108'):
                            Temploko = -3.6680555555
                        if str(temploko.get()).lower()[3:] in ('sojourner', 'soĵerner', 'soĵernor', 'soĝerner', 'soĝernor', 'vojaĝeganto', 'mars pathfinder', 'marz pathfinder', 'marsa vojtrovanto', 'marsvojtrovanto', 'mars-vojtrovanto', 'marsovojtrovanto', 'vojtrovanto de marso', 'vojtrovanto je marso', 'vojtrovanto marsa', '1996-068a', '24667' 'pathfinder', 'vojtrovanto'):
                            Temploko = 4.9630888889
                        if str(temploko.get()).lower()[3:] in ('perseverance', 'percy', 'persistemo', 'persistema', 'persevirans', 'persi', 'ingenuity', 'enĝeueti', 'inĝeueti', 'mars helicopter', 'helikoptero marsa', 'marsa helikoptero', 'marshelikoptero', 'mars-helikoptero', 'marsohelikoptero', 'eltrovemo', 'eltrovema'):
                            Temploko = -2.848588889
                        if str(temploko.get()).lower()[3:] in ('2018-042a', '43457', 'interior exploration using seismic investigations, geodesy and heat transport', 'interior exploration using seismic investigations, geodesy and heat transport (insight)', 'interior exploration using seismic investigations, geodesy, and heat transport', 'interior exploration using seismic investigations, geodesy, and heat transport (insight)', 'interior exploration using seismic investigations geodesy and heat transport', 'interior exploration using seismic investigations geodesy and heat transport (insight)', 'ensajt'):
                            Temploko = -1.2326833333
                        if str(temploko.get()).lower()[3:] in ('curiosity', 'scivolemo', 'scivolema', 'scivolemaĵo', 'scivolemulo', 'kuriozo', 'kurioza', 'kuriozaĵo', 'mars science laboratory', 'msl', '37936', '2011-070a', 'marsa scienca laboratorio', 'scienca laboratorio marsa'):
                            Temploko = -1.1827777778
                        if str(temploko.get()).lower()[3:] in ('opportunity', 'mer-b', 'merb', 'mars exploration rover b', 'esplorada vaganta veturilo marsa b', 'marsa esplorada vaganta veturilo b', 'esplorada veturilo marsa b', 'marsa esplorada veturilo b', 'marz eksplorejŝen rover b', 'marz eksplorejŝan rover b', 'oportjuneti', 'oportuneti', '2003-032a', '27849'):
                            Temploko = 4.8464833333
                        if str(temploko.get()).lower()[3:] in ('spirit', 'spirito', 'animo', 'mer-a', 'mera', 'mars exploration rover a', 'esplorada vaganta veturilo marsa a', 'marsa esplorada vaganta veturilo a', 'esplorada veturilo marsa a', 'marsa esplorada veturilo a', 'marz eksplorejŝen rover a', 'marz eksplorejŝan rover a' '2003-027a', '27827'):
                            Temploko = -0.125760111
                        if str(temploko.get()).lower()[3:] in ('марс-6', 'марс6', 'марс 6', 'м-73п 50', 'mars-6', 'mars 6', 'marz-6', 'marz6', 'marsa 6', 'marsa-6', 'marsa6', 'marsa sesa', 'sesa marsa', 'marso 6', 'marso-6', 'marso6', 'marso sesa', 'sesa marso', '7223', '6768', '1973-052d', '1973-052a'):
                            Temploko = 4.4605555556
                        if str(temploko.get()).lower()[3:] in ('марс-3', 'марс3', 'марс 3', 'mars-3', 'mars 3', 'marz-3', 'marz3', 'marsa 3', 'marsa-3', 'marsa3', 'marsa tria', 'tria marsa', 'marso 3', 'marso-3', 'marso3', 'marso tria', 'tria marso', '5667', '5252', '1971-049c', '1971-049a'):
                            Temploko = -0.6111111111
                    elif str(temploko.get()).lower()[0:3] in ('male', 'mal ', 'mal-', 'mala', '180 ', '180-', '200 ', '200-'):
                        if str(temploko.get()).lower()[4:] in ('phoenix', 'fenikso', 'feniks', '2007-034a', '32003'):
                            Temploko = 1.50833333
                        if str(temploko.get()).lower()[4:] in ('viking 2', 'viking two', 'viking2', 'vikingtwo', 'vajkeng 2', 'vajkeng du', 'vajkeng tu', 'vajkeng2', 'vajkengdu', 'vajkengtu', 'vajking 2', 'vajking du', 'vajking tu', 'vajking2', 'vajkingdu', 'vajkingtu', 'vikingo 2a', '2a vikingo', 'vikingo2a', '2avikingo', 'vikingo 2', 'vikingo du', 'vikingo dua', 'vikingo2', 'vikingodu', 'vikingodua', 'dua vikingo', 'duavikingo', '1975-083c', '9408', '1975-083a', '8199'):
                            Temploko = -1.2697222222
                        if str(temploko.get()).lower()[4:] in ('viking 1', 'viking one', 'viking1', 'vikingone', 'vajkeng 1', 'vajkeng unu', 'vajkeng', 'vajkeng1', 'vajkengunu', 'vajking 1', 'vajking unu', 'vajking1', 'vajkingunu', 'vikingo', 'vikingo 1', 'vikingo 1a', '1a vikingo', 'vikingo1a', '1avikingo', 'vikingo unu', 'vikingo unua', 'vikingo1', 'vikingounu', 'vikingounua', 'unua vikingo', 'unuavikingo', '1975-075c', '9024', '1975-075a', '8108'):
                            Temploko = -3.6680555555
                        if str(temploko.get()).lower()[4:] in ('sojourner', 'soĵerner', 'soĵernor', 'soĝerner', 'soĝernor', 'vojaĝeganto', 'mars pathfinder', 'marz pathfinder', 'marsa vojtrovanto', 'marsvojtrovanto', 'mars-vojtrovanto', 'marsovojtrovanto', 'vojtrovanto de marso', 'vojtrovanto je marso', 'vojtrovanto marsa', '1996-068a', '24667' 'pathfinder', 'vojtrovanto'):
                            Temploko = 4.9630888889
                        if str(temploko.get()).lower()[4:] in ('perseverance', 'percy', 'persistemo', 'persistema', 'persevirans', 'persi', 'ingenuity', 'enĝeueti', 'inĝeueti', 'mars helicopter', 'helikoptero marsa', 'marsa helikoptero', 'marshelikoptero', 'mars-helikoptero', 'marsohelikoptero', 'eltrovemo', 'eltrovema'):
                            Temploko = -2.8491666667
                        if str(temploko.get()).lower()[4:] in ('2018-042a', '43457', 'interior exploration using seismic investigations, geodesy and heat transport', 'interior exploration using seismic investigations, geodesy and heat transport (insight)', 'interior exploration using seismic investigations, geodesy, and heat transport', 'interior exploration using seismic investigations, geodesy, and heat transport (insight)', 'interior exploration using seismic investigations geodesy and heat transport', 'interior exploration using seismic investigations geodesy and heat transport (insight)', 'ensajt'):
                            Temploko = -1.2326833333
                        if str(temploko.get()).lower()[4:] in ('curiosity', 'scivolemo', 'scivolema', 'scivolemaĵo', 'scivolemulo', 'kuriozo', 'kurioza', 'kuriozaĵo', 'mars science laboratory', 'msl', '37936', '2011-070a', 'marsa scienca laboratorio', 'scienca laboratorio marsa'):
                            Temploko = -1.1827777778
                        if str(temploko.get()).lower()[4:] in ('opportunity', 'mer-b', 'merb', 'mars exploration rover b', 'esplorada vaganta veturilo marsa b', 'marsa esplorada vaganta veturilo b', 'esplorada veturilo marsa b', 'marsa esplorada veturilo b', 'marz eksplorejŝen rover b', 'marz eksplorejŝan rover b', 'oportjuneti', 'oportuneti', '2003-032a', '27849'):
                            Temploko = 4.8464833333
                        if str(temploko.get()).lower()[4:] in ('spirit', 'spirito', 'animo', 'mer-a', 'mera', 'mars exploration rover a', 'esplorada vaganta veturilo marsa a', 'marsa esplorada vaganta veturilo a', 'esplorada veturilo marsa a', 'marsa esplorada veturilo a', 'marz eksplorejŝen rover a', 'marz eksplorejŝan rover a' '2003-027a', '27827'):
                            Temploko = -0.125760111
                        if str(temploko.get()).lower()[4:] in ('марс-6', 'марс6', 'марс 6', 'м-73п 50', 'mars-6', 'mars 6', 'marz-6', 'marz6', 'marsa 6', 'marsa-6', 'marsa6', 'marsa sesa', 'sesa marsa', 'marso 6', 'marso-6', 'marso6', 'marso sesa', 'sesa marso', '7223', '6768', '1973-052d', '1973-052a'):
                            Temploko = 4.4605555556
                        if str(temploko.get()).lower()[4:] in ('марс-3', 'марс3', 'марс 3', 'mars-3', 'mars 3', 'marz-3', 'marz3', 'marsa 3', 'marsa-3', 'marsa3', 'marsa tria', 'tria marsa', 'marso 3', 'marso-3', 'marso3', 'marso tria', 'tria marso', '5667', '5252', '1971-049c', '1971-049a'):
                            Temploko = -0.6111111111
                    elif str(temploko.get()).lower()[0:4] in ('male ', 'mal- ', 'mala '):
                        if str(temploko.get()).lower()[5:] in ('phoenix', 'fenikso', 'feniks', '2007-034a', '32003'):
                            Temploko = 1.50833333
                        if str(temploko.get()).lower()[5:] in ('viking 2', 'viking two', 'viking2', 'vikingtwo', 'vajkeng 2', 'vajkeng du', 'vajkeng tu', 'vajkeng2', 'vajkengdu', 'vajkengtu', 'vajking 2', 'vajking du', 'vajking tu', 'vajking2', 'vajkingdu', 'vajkingtu', 'vikingo 2a', '2a vikingo', 'vikingo2a', '2avikingo', 'vikingo 2', 'vikingo du', 'vikingo dua', 'vikingo2', 'vikingodu', 'vikingodua', 'dua vikingo', 'duavikingo', '1975-083c', '9408', '1975-083a', '8199'):
                            Temploko = -1.2697222222
                        if str(temploko.get()).lower()[5:] in ('viking 1', 'viking one', 'viking1', 'vikingone', 'vajkeng 1', 'vajkeng unu', 'vajkeng', 'vajkeng1', 'vajkengunu', 'vajking 1', 'vajking unu', 'vajking1', 'vajkingunu', 'vikingo', 'vikingo 1', 'vikingo 1a', '1a vikingo', 'vikingo1a', '1avikingo', 'vikingo unu', 'vikingo unua', 'vikingo1', 'vikingounu', 'vikingounua', 'unua vikingo', 'unuavikingo', '1975-075c', '9024', '1975-075a', '8108'):
                            Temploko = -3.6680555555
                        if str(temploko.get()).lower()[5:] in ('sojourner', 'soĵerner', 'soĵernor', 'soĝerner', 'soĝernor', 'vojaĝeganto', 'mars pathfinder', 'marz pathfinder', 'marsa vojtrovanto', 'marsvojtrovanto', 'mars-vojtrovanto', 'marsovojtrovanto', 'vojtrovanto de marso', 'vojtrovanto je marso', 'vojtrovanto marsa', '1996-068a', '24667' 'pathfinder', 'vojtrovanto'):
                            Temploko = 4.9630888889
                        if str(temploko.get()).lower()[5:] in ('perseverance', 'percy', 'persistemo', 'persistema', 'persevirans', 'persi', 'ingenuity', 'enĝeueti', 'inĝeueti', 'mars helicopter', 'helikoptero marsa', 'marsa helikoptero', 'marshelikoptero', 'mars-helikoptero', 'marsohelikoptero', 'eltrovemo', 'eltrovema'):
                            Temploko = -2.8491666667
                        if str(temploko.get()).lower()[5:] in ('2018-042a', '43457', 'interior exploration using seismic investigations, geodesy and heat transport', 'interior exploration using seismic investigations, geodesy and heat transport (insight)', 'interior exploration using seismic investigations, geodesy, and heat transport', 'interior exploration using seismic investigations, geodesy, and heat transport (insight)', 'interior exploration using seismic investigations geodesy and heat transport', 'interior exploration using seismic investigations geodesy and heat transport (insight)', 'ensajt'):
                            Temploko = -1.2326833333
                        if str(temploko.get()).lower()[5:] in ('curiosity', 'scivolemo', 'scivolema', 'scivolemaĵo', 'scivolemulo', 'kuriozo', 'kurioza', 'kuriozaĵo', 'mars science laboratory', 'msl', '37936', '2011-070a', 'marsa scienca laboratorio', 'scienca laboratorio marsa'):
                            Temploko = -1.1827777778
                        if str(temploko.get()).lower()[5:] in ('opportunity', 'mer-b', 'merb', 'mars exploration rover b', 'esplorada vaganta veturilo marsa b', 'marsa esplorada vaganta veturilo b', 'esplorada veturilo marsa b', 'marsa esplorada veturilo b', 'marz eksplorejŝen rover b', 'marz eksplorejŝan rover b', 'oportjuneti', 'oportuneti', '2003-032a', '27849'):
                            Temploko = 4.8464833333
                        if str(temploko.get()).lower()[5:] in ('spirit', 'spirito', 'animo', 'mer-a', 'mera', 'mars exploration rover a', 'esplorada vaganta veturilo marsa a', 'marsa esplorada vaganta veturilo a', 'esplorada veturilo marsa a', 'marsa esplorada veturilo a', 'marz eksplorejŝen rover a', 'marz eksplorejŝan rover a' '2003-027a', '27827'):
                            Temploko = -0.125760111
                        if str(temploko.get()).lower()[5:] in ('марс-6', 'марс6', 'марс 6', 'м-73п 50', 'mars-6', 'mars 6', 'marz-6', 'marz6', 'marsa 6', 'marsa-6', 'marsa6', 'marsa sesa', 'sesa marsa', 'marso 6', 'marso-6', 'marso6', 'marso sesa', 'sesa marso', '7223', '6768', '1973-052d', '1973-052a'):
                            Temploko = 4.4605555556
                        if str(temploko.get()).lower()[5:] in ('марс-3', 'марс3', 'марс 3', 'mars-3', 'mars 3', 'marz-3', 'marz3', 'marsa 3', 'marsa-3', 'marsa3', 'marsa tria', 'tria marsa', 'marso 3', 'marso-3', 'marso3', 'marso tria', 'tria marso', '5667', '5252', '1971-049c', '1971-049a'):
                            Temploko = -0.6111111111
                    else:
                        if str(temploko.get()).lower() in ('phoenix', 'fenikso', 'feniks', '2007-034a', '32003'):
                            Temploko = -3.4916666667
                        if str(temploko.get()).lower() in ('viking 2', 'viking two', 'viking2', 'vikingtwo', 'vajkeng 2', 'vajkeng du', 'vajkeng tu', 'vajkeng2', 'vajkengdu', 'vajkengtu', 'vajking 2', 'vajking du', 'vajking tu', 'vajking2', 'vajkingdu', 'vajkingtu', 'vikingo 2a', '2a vikingo', 'vikingo2a', '2avikingo', 'vikingo 2', 'vikingo du', 'vikingo dua', 'vikingo2', 'vikingodu', 'vikingodua', 'dua vikingo', 'duavikingo', '1975-083c', '9408', '1975-083a', '8199'):
                            Temploko = 3.7302777778
                        if str(temploko.get()).lower() in ('viking 1', 'viking one', 'viking1', 'vikingone', 'vajkeng 1', 'vajkeng unu', 'vajkeng', 'vajkeng1', 'vajkengunu', 'vajking 1', 'vajking unu', 'vajking1', 'vajkingunu', 'vikingo', 'vikingo 1', 'vikingo 1a', '1a vikingo', 'vikingo1a', '1avikingo', 'vikingo unu', 'vikingo unua', 'vikingo1', 'vikingounu', 'vikingounua', 'unua vikingo', 'unuavikingo', '1975-075c', '9024', '1975-075a', '8108'):
                            Temploko = 1.319444444
                        if str(temploko.get()).lower() in ('sojourner', 'soĵerner', 'soĵernor', 'soĝerner', 'soĝernor', 'vojaĝeganto', 'mars pathfinder', 'marz pathfinder', 'marsa vojtrovanto', 'marsvojtrovanto', 'mars-vojtrovanto', 'marsovojtrovanto', 'vojtrovanto de marso', 'vojtrovanto je marso', 'vojtrovanto marsa', '1996-068a', '24667' 'pathfinder', 'vojtrovanto'):
                            Temploko = -0.0369111111
                        if str(temploko.get()).lower() in ('perseverance', 'percy', 'persistemo', 'persistema', 'persevirans', 'persi', 'ingenuity', 'enĝeueti', 'inĝeueti', 'mars helicopter', 'helikoptero marsa', 'marsa helikoptero', 'marshelikoptero', 'mars-helikoptero', 'marsohelikoptero', 'eltrovemo', 'eltrovema'):
                            Temploko = 2.1508333333
                        if str(temploko.get()).lower() in ('2018-042a', '43457', 'interior exploration using seismic investigations, geodesy and heat transport', 'interior exploration using seismic investigations, geodesy and heat transport (insight)', 'interior exploration using seismic investigations, geodesy, and heat transport', 'interior exploration using seismic investigations, geodesy, and heat transport (insight)', 'interior exploration using seismic investigations geodesy and heat transport', 'interior exploration using seismic investigations geodesy and heat transport (insight)', 'ensajt'):
                            Temploko = 3.7673166667
                        if str(temploko.get()).lower() in ('curiosity', 'scivolemo', 'scivolema', 'scivolemaĵo', 'scivolemulo', 'kuriozo', 'kurioza', 'kuriozaĵo', 'mars science laboratory', 'msl', '37936', '2011-070a', 'marsa scienca laboratorio', 'scienca laboratorio marsa'):
                            Temploko = 3.8172222222
                        if str(temploko.get()).lower() in ('opportunity', 'mer-b', 'merb', 'mars exploration rover b', 'esplorada vaganta veturilo marsa b', 'marsa esplorada vaganta veturilo b', 'esplorada veturilo marsa b', 'marsa esplorada veturilo b', 'marz eksplorejŝen rover b', 'marz eksplorejŝan rover b', 'oportjuneti', 'oportuneti', '2003-032a', '27849'):
                            Temploko = -0.1535166667
                        if str(temploko.get()).lower() in ('spirit', 'spirito', 'animo', 'mer-a', 'mera', 'mars exploration rover a', 'esplorada vaganta veturilo marsa a', 'marsa esplorada vaganta veturilo a', 'esplorada veturilo marsa a', 'marsa esplorada veturilo a', 'marz eksplorejŝen rover a', 'marz eksplorejŝan rover a' '2003-027a', '27827'):
                            Temploko = 4.87423989
                        if str(temploko.get()).lower() in ('марс-6', 'марс6', 'марс 6', 'м-73п 50', 'mars-6', 'mars 6', 'marz-6', 'marz6', 'marsa 6', 'marsa-6', 'marsa6', 'marsa sesa', 'sesa marsa', 'marso 6', 'marso-6', 'marso6', 'marso sesa', 'sesa marso', '7223', '6768', '1973-052d', '1973-052a'):
                            Temploko = -0.5394444444
                        if str(temploko.get()).lower() in ('марс-3', 'марс3', 'марс 3', 'mars-3', 'mars 3', 'marz-3', 'marz3', 'marsa 3', 'marsa-3', 'marsa3', 'marsa tria', 'tria marsa', 'marso 3', 'marso-3', 'marso3', 'marso tria', 'tria marso', '5667', '5252', '1971-049c', '1971-049a'):
                            Temploko = 4.3888888889
                except Exception:
                    pass
            Temploko = int(str(temploko.get()))
            Longo_ = int(str(longo.get()).replace('kutime', '0').replace('pli', '1').replace('malpli', '-1'))
            if montru.get().lower() in ('5', 'kvin'):
                Montru_ = 5
            elif montru.get().lower() in ('1', 'unu', 'un'):
                Montru_ = 1
            elif montru.get().lower() in ('2', 'du'):
                Montru_ = 2
            elif montru.get().lower() in ('3', 'tri'):
                Montru_ = 3
            elif montru.get().lower() in ('4', 'kvar'):
                Montru_ = 4
            else:
                Montru_ = 5
            Kio_ = kio.get()

            Longo = Longo_
            Montru = Montru_
            if Kio_ == 'nekutiman':
                Kio = 2
            else:
                Kio = 1
        except Exception:
            pass
        if Loko_.lower() in ('La Tero', 'la tero', 'ltero', 'LTero', 'Ltero', '399', '350'):
            JaroGregorie = 1792
            MonatoGregorie = 9
            TagoGregorie = 22
            UTCa_horoj = 0
            UTCa_minutoj = 0
            UTCa_s = 0
            UTCa_ms = 0
            Enŝovo = 1
            NeenŝovaLongo = 365
            UnuaJararaLongo = 4
            moduloUnua = "'0'"
            DuaJararaLongo = 128
            moduloDua = "'0'"
            MilisekundojPerTagumo = 86400000
            Loko = 'La Tero'
            KiamEnŝoviguJaron = 0
            JarDekonaLongo = 40
            DekaJardekonaLongo = 5
            ĈuFaruMonaton = 1
            MonataLongo = 30
            FinaMonataLongo = 5
            Praviganto1 = 54
        if  Loko_.lower() in ('La Luno', 'la luno', 'lluno', 'LLuno', 'Lluno', '301'):
            JaroGregorie = 1792
            MonatoGregorie = 9
            TagoGregorie = 1
            UTCa_horoj = 6
            UTCa_minutoj = 26
            UTCa_s = 0#eble ne
            UTCa_ms = 0#eble ne
            Enŝovo = 1#?
            NeenŝovaLongo = 618#.4136286614534  170 de 411
            UnuaJararaLongo = 0#?
            DuaJararaLongo = 0#?
            MilisekundojPerTagumo = 51028857.792 # 2 % de vera tagumo
            Loko = 'La Luno'
        if Loko_.lower() in ('Marso', 'marso', '499', '450'):
            JaroGregorie = 1956
            MonatoGregorie = 4
            TagoGregorie = 26
            UTCa_horoj = 20
            UTCa_minutoj = 8
            UTCa_s = 46
            UTCa_ms = 0
            Enŝovo = -1
            # 372 tagnoktumoj malpli ol laŭ http://www-mars.lmd.jussieu.fr/mars/time/martian_time.html
            NeenŝovaLongo = 669
            UnuaJararaLongo = 5
            moduloUnua = "'1', '3'" # enŝovigas la jaron
            DuaJararaLongo = 125
            moduloDua = "'0'" # enŝovigas la jaron
            MilisekundojPerTagumo = 88775266.24205198208
            ĈuFaruMonaton = 0
            Loko = 'Marso'
            KiamEnŝoviguJaron = 0
            JarDekonaLongo = 70
            DekaJardekonaLongo = 35

        if UnuaJararaLongo > DuaJararaLongo:
            UnuaJararaLongo, DuaJararaLongo = DuaJararaLongo, UnuaJararaLongo
        if DuaJararaLongo % UnuaJararaLongo == 0:
            PlejMalsupraKomunaOblo = DuaJararaLongo
        else:
            PlejMalsupraKomunaOblo = DuaJararaLongo + 1
        AlmenaŭĈiam = 1
        while AlmenaŭĈiam == 1:
            if PlejMalsupraKomunaOblo % UnuaJararaLongo == PlejMalsupraKomunaOblo % DuaJararaLongo == 0:
                AlmenaŭĈiam = 0
            PlejMalsupraKomunaOblo += 1
        EnŝovaLongo = Enŝovo + NeenŝovaLongo
        TagumojPerUnuaJararo = Enŝovo + UnuaJararaLongo * NeenŝovaLongo
        TagumojPerDuaJararo = DuaJararaLongo / UnuaJararaLongo * TagumojPerUnuaJararo - Enŝovo
        EpoĥoGregorie = datetime(JaroGregorie, MonatoGregorie, TagoGregorie)
        UniksoGregorie = datetime(1970, 1, 1)
        Intero = UniksoGregorie - EpoĥoGregorie
        TagojInterEpoĥoKajUnikso = Intero.days
        Minuss = (UTCa_horoj + UTCa_minutoj + UTCa_s + UTCa_ms) * 1000
        msInterEpoĥoKajUnikso = TagojInterEpoĥoKajUnikso * 86400000 - Minuss
        TagumojInterEpoĥoKajUnikso = msInterEpoĥoKajUnikso / MilisekundojPerTagumo

        Vortoj = LaTagumero(Unikso, MilisekundojPerTagumo, TagumojInterEpoĥoKajUnikso,
                            TagumojPerUnuaJararo, moduloUnua, TagumojPerDuaJararo, moduloDua,
                            PlejMalsupraKomunaOblo, EnŝovaLongo, NeenŝovaLongo, KiamEnŝoviguJaron,
                            JarDekonaLongo, DekaJardekonaLongo, ĈuFaruMonaton, MonataLongo,
                            FinaMonataLongo, Temploko, Loko, Longo, PliĜustigo, Kio, Montru,
                            Praviganto1, Praviganto2)
        V.set(Vortoj)
##        Kaŝu()
        root.update()

root.after(20, Funkciigu)
root.mainloop()
